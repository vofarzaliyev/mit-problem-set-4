# Problem Set 4C
# Name: <your name here>
# Collaborators:
# Time Spent: x:xx

import string
from ps4a import get_permutations
from copy import deepcopy

### HELPER CODE ###
def load_words(file_name):
    '''
    file_name (string): the name of the file containing 
    the list of words to load    
    
    Returns: a list of valid words. Words are strings of lowercase letters.
    
    Depending on the size of the word list, this function may
    take a while to finish.
    '''
    
    print("Loading word list from file...")
    # inFile: file
    inFile = open(file_name, 'r')
    # wordlist: list of strings
    wordlist = []
    for line in inFile:
        wordlist.extend([word.lower() for word in line.split(' ')])
    print("  ", len(wordlist), "words loaded.")
    return wordlist

def is_word(word_list, word):
    '''
    Determines if word is a valid word, ignoring
    capitalization and punctuation

    word_list (list): list of words in the dictionary.
    word (string): a possible word.
    
    Returns: True if word is in word_list, False otherwise

    Example:
    >>> is_word(word_list, 'bat') returns
    True
    >>> is_word(word_list, 'asdf') returns
    False
    '''
    word = word.lower()
    word = word.strip(" !@#$%^&*()-_+={}[]|\:;'<>?,./\"")
    return word in word_list


### END HELPER CODE ###

WORDLIST_FILENAME = 'words.txt'

# you may find these constants helpful
VOWELS_LOWER = 'aeiou'
VOWELS_UPPER = 'AEIOU'
CONSONANTS_LOWER = 'bcdfghjklmnpqrstvwxyz'
CONSONANTS_UPPER = 'BCDFGHJKLMNPQRSTVWXYZ'

class SubMessage(object):
    def __init__(self, text):
        self.message_text = text
        self.valid_words = load_words('words.txt')
            
    def get_message_text(self):
        return self.message_text

    def get_valid_words(self):
        return deepcopy(self.valid_words)

    def build_transpose_dict(self, vowels_permutation):
        return dict(dict(dict(zip(list(VOWELS_LOWER),list(vowels_permutation.lower()))),**dict(zip(list(CONSONANTS_LOWER),list(CONSONANTS_LOWER))),**dict(dict(zip(list(VOWELS_UPPER),list(vowels_permutation.upper())))),**dict(zip(list(CONSONANTS_UPPER),list(CONSONANTS_UPPER)))))  

    def apply_transpose(self, transpose_dict):
        encrypted_message = ''
        for i in range(len(self.message_text)):
            if self.message_text[i] in transpose_dict.keys():
                encrypted_message += transpose_dict[self.message_text[i]] 
            else:
                encrypted_message += self.message_text[i] 
        return encrypted_message


class EncryptedSubMessage(SubMessage):
    def __init__(self, text):
        SubMessage.__init__(self,text)

    def decrypt_message(self):
        decryption_attempts = {}
        for permutation in get_permutations(VOWELS_LOWER):
            attempt = self.apply_transpose(self.build_transpose_dict(permutation)).split()
            decryption_attempts[permutation] = len([x for x in attempt if is_word(self.valid_words,x)])
        return self.apply_transpose(self.build_transpose_dict(max(decryption_attempts,key=decryption_attempts.get)))    

if __name__ == '__main__':

    # Example test case
    message = SubMessage("Hello World!")
    permutation = "eaiuo"
    enc_dict = message.build_transpose_dict(permutation)
    print("Original message:", message.get_message_text(), "Permutation:", permutation)
    print("Expected encryption:", "Hallu Wurld!")
    print("Actual encryption:", message.apply_transpose(enc_dict))
    enc_message = EncryptedSubMessage(message.apply_transpose(enc_dict))
    print("Decrypted message:", enc_message.decrypt_message())
     
    #TODO: WRITE YOUR TEST CASES HERE
