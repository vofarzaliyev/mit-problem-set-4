# Problem Set 4A
# Name: <your name here>
# Collaborators:
# Time Spent: x:xx
from functools import reduce


def get_permutations(sequence):
    if len(sequence)==1:
        return [sequence]
    else:
        fornow = []
        for element in get_permutations(sequence[1:]):
            fornow.append([element[:i]+sequence[0]+element[i:] for i in range(len(element)+1)])
        final_permutations = reduce(lambda a,b:a+b,fornow)
        return list(set(final_permutations))



print(get_permutations('aaaa'))

if __name__ == '__main__':
    example_input = 'abc'
    print('Input:', example_input)
    print('Expected Output:', ['abc', 'acb', 'bac', 'bca', 'cab', 'cba'])
    print('Actual Output:', get_permutations(example_input))
    
#    # Put three example test cases here (for your sanity, limit your inputs
#    to be three characters or fewer as you will have n! permutations for a 
#    sequence of length n)

     #delete this line and replace with your code here

