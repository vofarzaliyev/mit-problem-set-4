# Problem Set 4B
# Name: <your name here>
# Collaborators:
# Time Spent: x:xx

import string
from copy import deepcopy

### HELPER CODE ###
def load_words(file_name):
    '''
    file_name (string): the name of the file containing 
    the list of words to load    
    
    Returns: a list of valid words. Words are strings of lowercase letters.
    
    Depending on the size of the word list, this function may
    take a while to finish.
    '''
    print("Loading word list from file...")
    # inFile: file
    inFile = open(file_name, 'r')
    # wordlist: list of strings
    wordlist = []
    for line in inFile:
        wordlist.extend([word.lower() for word in line.split(' ')])
    print("  ", len(wordlist), "words loaded.")
    return wordlist

def is_word(word_list, word):
    '''
    Determines if word is a valid word, ignoring
    capitalization and punctuation

    word_list (list): list of words in the dictionary.
    word (string): a possible word.
    
    Returns: True if word is in word_list, False otherwise

    Example:
    >>> is_word(word_list, 'bat') returns
    True
    >>> is_word(word_list, 'asdf') returns
    False
    '''
    word = word.lower()
    word = word.strip(" !@#$%^&*()-_+={}[]|\:;'<>?,./\"")
    return word in word_list

def get_story_string():
    """
    Returns: a story in encrypted text.
    """
    f = open("story.txt", "r")
    story = str(f.read())
    f.close()
    return story

### END HELPER CODE ###

WORDLIST_FILENAME = 'words.txt'

class Message(object):
    def __init__(self, text):
        self.message_text = text
        self.valid_words = load_words('words.txt')

    def get_message_text(self):
        return self.message_text

    def get_valid_words(self):
        return deepcopy(self.valid_words)

    def build_shift_dict(self,shift):
        return dict(dict(zip(string.ascii_lowercase,string.ascii_lowercase[shift:]+string.ascii_lowercase[:shift])), **dict(zip(string.ascii_uppercase,string.ascii_uppercase[shift:]+string.ascii_uppercase[:shift])))
 
    def apply_shift(self, shift):
        encrypted_message = ''
        for i in range(len(self.message_text)):
            if self.message_text[i] in self.build_shift_dict(shift).keys():
                encrypted_message += self.build_shift_dict(shift)[self.message_text[i]]
            else:
                encrypted_message +=self.message_text[i]
        return encrypted_message


class PlaintextMessage(Message):
    def __init__(self, text, shift):
        Message.__init__(self,text)
        self.shift = shift
        self.encryption_dict = self.build_shift_dict(self.shift)
        self.message_text_encrypted = self.apply_shift(self.shift)

    def get_shift(self):
        return self.shift

    def get_encryption_dict(self):
        return deepcopy(self.encryption_dict)

    def get_message_text_encrypted(self):
        return self.message_text_encrypted

    def change_shift(self, shift):
        self.shift = shift
        self.message_text_encrypted = self.apply_shift(self.shift)


class CiphertextMessage(Message):
    def __init__(self, text):
        Message.__init__(self,text)

    def decrypt_message(self):
        shift_values = {}
        for i in range(1,27):
            list_of_words_encrypted = PlaintextMessage.apply_shift(self,i)
            shift_values[i] = len([x for x in list_of_words_encrypted.split() if is_word(self.valid_words,x)])
        return (max(shift_values,key=shift_values.get),PlaintextMessage.apply_shift(self,max(shift_values,key=shift_values.get)))


if __name__ == '__main__':

#    #Example test case (PlaintextMessage)
    plaintext = PlaintextMessage('hello', 2)
    print('Expected Output: jgnnq')
    print('Actual Output:', plaintext.get_message_text_encrypted())
#
#    #Example test case (CiphertextMessage)
    ciphertext = CiphertextMessage('jgnnq')
    print('Expected Output:', (24, 'hello'))
    print('Actual Output:', ciphertext.decrypt_message())

    #TODO: WRITE YOUR TEST CASES HERE
a = Message('heeey')
a.apply_shift(3)
print(a.get_message_text())
    #TODO: best shift value and unencrypted story 
    
